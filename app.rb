
get '/' do
  haml :index
end


post '/remember' do
  message_text = "New Beta tester:\n#{params[:mail]}"
  message = <<EOF
From: RubyBots <beta-testers@rubybots.com>
To: Sergey Gerasimov <mail@grsmv.com>
Subject: Ruby Bots > New Beta Tester
#{message_text}
EOF

  Net::SMTP.start('localhost') do |smtp|
    smtp.send_message(
      message,
      'beta-testers@rubybots.com',
      'mail@grsmv.com')
  end

  'ok'
end
