require "rubygems"
require "sinatra"
# require "sinatra/reloader"
require "sinatra/cache"
require 'net/smtp'

# All this stuff needed in production:
root_dir = File.expand_path(File.dirname(__FILE__))
set :environment,       :development
set :root,              root_dir
set :public,            File.expand_path(File.dirname(__FILE__) + '/public')
set :views,             File.expand_path(File.dirname(__FILE__) + '/views')
set :app_file,          File.join(root_dir, 'app.rb')
set :cache_enabled,     true
set :cache_environment, :development

FileUtils.mkdir_p 'log' unless File.exists?('log')
log = File.new('log/sinatra.log', 'a')
$stdout.reopen(log)
$stderr.reopen(log)

disable :run

require "app.rb"
run Sinatra::Application
