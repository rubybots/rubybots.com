function terminal_timer() {
  var term = $('div#terminal');
  term.is(':visible') ?
    term.fadeOut(400) :
    term.fadeIn(400);
}

$(document).ready(function(){

  terminal_interval = setInterval("terminal_timer()", 800);

  $('input#mail').focusin(function()
  {
    $('div#terminal').fadeOut();
    clearInterval(terminal_interval);
  }).focusout(function()
  {
    if($(this).val() == '')
    {
      $('div#terminal').fadeIn();
      terminal_interval = setInterval("terminal_timer()", 800);
    }
  });


  $('form#form').submit(function(){
    var mail = $('input#mail').val();

    if (mail != '') 
    {
      $.post('/remember', { mail:mail }, function(data){
        if(data == 'ok')
        {
          clearInterval(terminal_interval);
          $('div#mail-form').children().each(function(){
            $(this).fadeOut();
          })
          $('div#thanks').fadeIn();
        }
      });
    }

    return false;
  });


  $('span.help').hover(function(){
    $(this).prev('span.tooltip').fadeIn(100);
  }, function(){
    $(this).prev('span.tooltip').fadeOut(100);
  });


  $('div#submit input').focusin(function(){
    $(this).css('background-position', '-390px -139px')
  }).focusout(function(){
    $(this).css('background-position', '-390px -32px')
  });


  $(window).keypress(function(){
    $('input#mail').focus();
  });


  $('div#grsmv').hover(function(){
    $(this).find('div.hover').fadeIn();
  }, function(){
    $(this).find('div.hover').fadeOut();
  });

});
